;;;; 4cimg.lisp

(in-package #:4cimg)

(defconstant *base-path* "~/Pictures/4chan/")
(defconstant *pattern* (ppcre:create-scanner ".jpg$|.png$|.gif$|.jpeg$|.webm$"))
(defvar *img-elms*)

(defun has-img (node)
  (and (plump:element-p node) (ppcre:scan *pattern* (plump:get-attribute node "href"))))

(defun add-img (node)
  (push node *img-elms*))

(defun main ()
  (dolist (url (cdr sb-ext:*posix-argv*))
    (setf *img-elms* nil)
    (let* ((board-start (+ 1 (position #\/ url :start (search ".org" url))))
           (board-end (position #\/ url :start (+ 1 board-start)))
           (board (subseq url board-start board-end))
           (thread-start (+ 1 (position #\/ url :from-end t)))
           (thread (subseq url thread-start))
           (dir (concatenate 'string *base-path* board "/" thread))
           (parsed-http (plump:parse (drakma:http-request url))))
      (plump:traverse parsed-http #'add-img :test #'has-img)
      (dolist (elm *img-elms*)
        (let* ((link (concatenate 'string "http:" (plump:get-attribute elm "href")))
               (file-name (concatenate 'string dir (subseq link (position #\/ link :from-end t)))))
          (unless (probe-file file-name)
            (trivial-download:download link file-name :quiet t))))
      (format t "Downloaded images from ~S to ~S~&" (concatenate 'string board "/" thread) dir))))
