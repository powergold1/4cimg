# 4cimg

This little program downloads all images/webms from a list of 4chan
thread and saves them in ~/Pictures/4chan/BOARD/THREAD/.  It's
intended to be compiled and used as an executable.  It only works with
sbcl, because I didn't care about adding different kinds of argv's for
different compilers.  To compile it, execute `sbcl --load 4cimg.asd
--eval '(ql:quickload :4cimg)' --eval '(in-package :4cimg)' --eval
"(sb-ext:save-lisp-and-die \"4cimg\" :toplevel 'main :executable t)"`.
Then call ./4cimg with the links to the threads you want to download.
