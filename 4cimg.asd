;;;; 4cimg.asd

(asdf:defsystem #:4cimg
  :description "Download all images from a list of 4chan threads"
  :author "Leonhard Staut"
  :license  "WTFPL"
  :version "1.0.0"
  :depends-on (#:plump
        #:drakma
        #:trivial-download
        #:cl-ppcre)
  :serial t
  :components ((:file "package")
               (:file "4cimg")))
